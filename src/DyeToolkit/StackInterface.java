/*
 * Interface for Stack ADT
 */
package DyeToolkit;

/**
 *
 * @author Frank
 */
public interface StackInterface
{
    public boolean isEmpty();
    //Determines whether the stack is empty
    //PreCondition: None
    //PostCondition: Returns true if stack is empty otherwise returns false.
    
    public void popAll();
    //Removes all items from the stack
    //PreCondition: None
    //PostCondition: Stack is empty.
    
    public void push (Object newItem) throws StackException;
    //Adds and item to the top of the stack
    //PreCondition: newItem != && is valid Object
    //PostCondition: On success insertion is on top of stack
    //Exception: Throws StackException if stack is full or cannot perform insertion
    
    public Object pop() throws StackException;
    //Removes and returns the top of the stack
    //PreCondition: None
    //PostCondition: If the stack is not empty, the top of the stack is returned and removed.
    //Exception: StackException thrown if stack is empty.
    
    public Object peek() throws StackException;
    //Returns the top of the stack without removing it
    //PreCondition: None
    //PostCondition: If the stack is not empty the top of the stack is returned without removing it.
    //Exception: StackException thrown if stack is empty.
    
}
