/*
 * StackException is thrown when the stack operation fails
 */
package DyeToolkit;

/**
 *
 * @author Frank Dye
 */

public class StackException extends Exception
{

    public StackException()
    {
        super();
    }

    public StackException(String str)
    {
        super(str);
    }
}
