
package DyeToolkit;

/**
 *
 * @author Frank
 */

//ListOutOfBoundsException thrown when method tries to access index that is 
//out of range
public class ListIndexOutOfBoundsException extends IndexOutOfBoundsException 
{   
    public ListIndexOutOfBoundsException(String str)
    {
        super (str);
    }
}