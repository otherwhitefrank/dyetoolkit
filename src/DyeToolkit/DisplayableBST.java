/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

/**
 *
 * @author Frank
 */
public class DisplayableBST<T extends KeyedItem<KT>, KT extends Comparable<? super KT>> extends BinarySearchTree<T, KT>
{
    public DisplayableBST()
    {
        super();
    }  // end default constructor

    public DisplayableBST(T rootItem)
    {
        super(rootItem);
    }  // end constructor
    
    public void displayTree()
    {
        printTree(root, 0);
    }
    
    

    private void printTree(TreeNode<T> currNode, int indent)
    {
        if (!(currNode==null))
        {
            //Do right branch
            printTree(currNode.rightChild, indent+3);
           
            //Print item
            int printIndent = indent + currNode.item.toString().length();
            String format = new String();
            if (indent == 0)
            {
              format += "%s\n";
              
            }
            else
            {               
              format += "%" + printIndent + "s\n";              
            }
            
            System.out.printf(format, currNode.item);
                   
            //Do left branch
            printTree(currNode.leftChild, indent+3);
            
        }
    }
    
    
}
