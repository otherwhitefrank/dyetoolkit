/*
 * This is the base interface for all List based objects. It defines
 * the essential methods all List's must implement.
 */
package DyeToolkit;

    
/**
 *
 * @author Frank
 */
public interface ListInterface<E> 
{
    public boolean isEmpty();
    
    public int size();
    
    public void add(int index, E item)
        throws ListIndexOutOfBoundsException;
    
    public void remove(int index)
        throws ListIndexOutOfBoundsException;
    
    public Object get(int index)
        throws ListIndexOutOfBoundsException;
    
    public void removeAll();

    
}



