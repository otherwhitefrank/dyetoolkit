/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package lab5;

/**
 *
 * @author Frank
 */
public abstract class KeyedItem<KT extends Comparable<? super KT>>
{

    private KT searchKey;

    public KeyedItem(KT key)
    {
        searchKey = key;
    }  // end constructor

    public KT getKey()
    {
        return searchKey;
    } // end getKey
} // end KeyedItem
