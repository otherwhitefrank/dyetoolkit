/*
 * Implementation of StackADT as a reference based linked list
 */
package DyeToolkit;

/**
 *
 * @author Frank Dye
 */

public class StackReferenceBased implements StackInterface
{

    private Node<Object> top;
    
    public StackReferenceBased()
    {
        top = null;
    }
    
    @Override
    public boolean isEmpty()
    {
        //Determines whether the stack is empty
        //PreCondition: None
        //PostCondition: Returns true if stack is empty otherwise returns false.
        return top == null;
    }

    @Override
    public void popAll()
    {
        //Removes all items from the stack
        //PreCondition: None
        //PostCondition: Stack is empty.
        top = null;
    }

    @Override
    public void push(Object newItem) throws StackException
    {
        //Adds and item to the top of the stack
        //PreCondition: newItem != && is valid Object
        //PostCondition: On success insertion is on top of stack
        //Exception: Throws StackException if stack is full or cannot perform insertion
        top = new Node(newItem, top);
    }

    @Override
    public Object pop() throws StackException
    {
        //Removes and returns the top of the stack
        //PreCondition: None
        //PostCondition: If the stack is not empty, the top of the stack is returned and removed.
        //Exception: StackException thrown if stack is empty.
        
        if (!isEmpty())
        {
            Node<Object> temp = top;
            top = top.next;
            return temp.item;
        }
        else
        {
            throw new StackException("Stack Exception " + 
                                     "pop: stack empty");
        }
    }

    @Override
    public Object peek() throws StackException
    {
        //Returns the top of the stack without removing it
        //PreCondition: None
        //PostCondition: If the stack is not empty the top of the stack is returned without removing it.
        //Exception: StackException thrown if stack is empty.
        
        if (!isEmpty())
        {
            return top.item;
        }
        else
        {
            throw new StackException("Stack Exception " + 
                                     "peek: stack empty");
        }
    }
    
}

class Node<E>
{
    //Node definition for the linked list, uses two constructors to create only the node
    //or the node linked to another node

    E item;
    Node<E> next;

    Node(E newItem)
    {
        item = newItem;
        next = null;
    }

    Node(E newItem, Node<E> nextNode)
    {
        item = newItem;
        next = nextNode;
    }
}