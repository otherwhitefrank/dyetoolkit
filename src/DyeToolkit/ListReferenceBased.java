/*
 * Reference based implementation of List, AKA a linked-list
 * 
 */
package DyeToolkit;



/**
 *
 * @author Frank
 */
public class ListReferenceBased<E> implements ListInterface
{
    //The head of the List

    private Node<E> head;
    //Number of items currently in the list
    private int numItems;

    public ListReferenceBased()
    {
        //Initialize the linked-list
        head = null;
        numItems = 0;

    }

    /**
     *
     * @return true if empty else return false.
     */
    @Override
    public boolean isEmpty()
    {
        return numItems == 0;
    }

    @Override
    public int size()
    {
        return numItems;
    }

   

    private Node<E> find(int index)
    {
        Node curr = head;
        for (int i = 0; i < index; i++)
        {
            //Skips ahead in the linked list until we are at index
            curr = curr.next;
        }

        //curr equals the Node at index
        return curr;
    }

    @Override
    public void remove(int index) throws ListIndexOutOfBoundsException
    {
        if (index >= 0 && index < numItems)
        {
            if (index == 0)
            {
                //First node in the list, just set head = head.next
                head = head.next;
            }
            else
            {
                Node prev = find(index - 1);
                Node curr = prev.next;
                prev.next = curr.next;
            }
            //decrement numItems
            numItems--;
        }
        else
        {
            throw new ListIndexOutOfBoundsException("List index out of bounds on remove");
            
        }
    }

    @Override
    public E get(int index) throws ListIndexOutOfBoundsException
    {
        if (index >= 0 && index < numItems)
        {
            Node<E> curr = find(index);
            E dataItem = curr.item;
            return dataItem;
        }
        else
        {
            throw new ListIndexOutOfBoundsException("List index out of bounds on get");
                                                
        }
    }

    @Override
    public void removeAll()
    {
        head = null;
        numItems = 0;
    }

    @Override
    public void add(int index, Object item) throws ListIndexOutOfBoundsException
    {
        if (index >= 0 && index < numItems + 1)
        {
            if (index == 0)
            {
                //Then create the node and append it to the beginning of the list relinking as needed
                Node newNode = new Node(item, head);
                head = newNode;
            } else
            {
                Node prev = find(index - 1);

                //Insert the node after the node prev references
                Node newNode = new Node(item, prev.next);
                prev.next = newNode;
            }

        } else
        {
            //Out of index bounds
            throw new ListIndexOutOfBoundsException("List index out of bounds on add!");
        }
    }

}
class Node<E>
{

    E item;
    Node next;

    Node(E newItem)
    {
        item = newItem;
        next = null;
    }

    Node(E newItem, Node nextNode)
    {
        item = newItem;
        next = nextNode;
    }
}