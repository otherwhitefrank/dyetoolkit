/*
 * Basis for BinaryTree implementations
 */
package lab5;

/**
 *
 * @author Frank
 */

class TreeNode<T> 
{
    T item;
    TreeNode<T> leftChild;
    TreeNode<T> rightChild;
    
    public TreeNode(T newItem)
    {
        item = newItem;
        leftChild = null;
        rightChild = null;
    }
    
    public TreeNode(T newItem, TreeNode<T> left, TreeNode<T> right)
    {
        item = newItem;
        leftChild = left;
        rightChild = right;
    }
    
    
}

class TreeException extends RuntimeException
{
    public TreeException(String s)
    {
        super(s);
    }
}

public abstract class BinaryTreeBasis<T>
{
    protected TreeNode<T> root;
    
    public BinaryTreeBasis()
    {
        root = null;
    }
    
    public BinaryTreeBasis(T rootItem)
    {
        root = new TreeNode<T>(rootItem, null, null);
    }
    
    public boolean isEmpty()
    {
        return root == null;
    }
    
    public void makeEmpty()
    {
        root = null;
    }
    
    public T getRootItem() throws TreeException
    {
        if (root == null)
        {
            throw new TreeException("TreeException: Empty tree");
        }
        else
        {
            return root.item;
        }
        
    }
    
    public abstract void setRootItem(T newItem);
    
}
