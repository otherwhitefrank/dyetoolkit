/*
 * Implementation of StackADT using list interface
 */
package DyeToolkit;

/**
 *
 * @author Frank Dye
 */

public class StackListBased implements StackInterface
{

    private ListInterface list;
    
    public StackListBased()
    {
        list = new ListReferenceBased();
    }
    
    @Override
    public boolean isEmpty()
    {
        //Determines whether the stack is empty
        //PreCondition: None
        //PostCondition: Returns true if stack is empty otherwise returns false.
        return list.isEmpty();
    }

    @Override
    public void popAll()
    {
        //Removes all items from the stack
        //PreCondition: None
        //PostCondition: Stack is empty.
        list.removeAll();
    }

    @Override
    public void push(Object newItem) throws StackException
    {
        //Adds and item to the top of the stack
        //PreCondition: newItem != && is valid Object
        //PostCondition: On success insertion is on top of stack
        //Exception: Throws StackException if stack is full or cannot perform insertion
        list.add(0, newItem);
    }

    @Override
    public Object pop() throws StackException
    {
        //Removes and returns the top of the stack
        //PreCondition: None
        //PostCondition: If the stack is not empty, the top of the stack is returned and removed.
        //Exception: StackException thrown if stack is empty.
        
        if (!list.isEmpty())
        {
            Object temp = list.get(0);
            list.remove(0);
            return temp;
        }
        else
        {
            throw new StackException("Stack Exception " + 
                                     "pop: stack empty");
        }
    }

    @Override
    public Object peek() throws StackException
    {
        //Returns the top of the stack without removing it
        //PreCondition: None
        //PostCondition: If the stack is not empty the top of the stack is returned without removing it.
        //Exception: StackException thrown if stack is empty.
        
        if (!list.isEmpty())
        {
            return list.get(0);
        }
        else
        {
            throw new StackException("Stack Exception " + 
                                     "peek: stack empty");
        }
    }
    
}

