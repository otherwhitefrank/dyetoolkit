/*
 * This is a simple implementation of a list Array based. It's done with Object
 * because generic don't like to be implemented as arrays.
 */
package DyeToolkit;

/**
 *
 * @author Frank
 */
public class ListArrayBased implements ListInterface
{

    private final int MAX_LIST = 500;
    private Object items[];
    private int numItems;
    
    
    public ListArrayBased()
    {
        items = new Object[MAX_LIST];
        numItems = 0;
        
    }
    
    
    @Override
    public boolean isEmpty()
    {
        return numItems == 0;
    }

    @Override
    public int size()
    {
        return numItems;
    }

    @Override
    public void add(int index, Object item) throws ListIndexOutOfBoundsException
    {
        if (numItems > MAX_LIST )
        {
            throw new ListException("ListException on add, list full!");
        }
        
        if (index >= 0 && index <= numItems)
        {
            //Shift all items up one
            for (int pos = numItems; pos>= index; pos--)
            {
                items[pos+1] = items[pos];
            }
            
            items[index] = item;
            numItems++;
            
            
            
        }
        else
        {
            throw new ListIndexOutOfBoundsException("ListIndexOutOfBoundsException on add");
            
        }
            
    }

    @Override
    public void remove(int index) throws ListIndexOutOfBoundsException
    {
        if (index >= 0 && index < numItems)
        {
            for (int pos = index +1; pos <= size(); pos++)
            {
                items[pos-1] = items[pos];
            }
            
            numItems--;
        }
        else
        {
            throw new ListIndexOutOfBoundsException("ListIndexOutOfBoundsException on remove");
        }
    }

    @Override
    public Object get(int index) throws ListIndexOutOfBoundsException
    {
        if (index >= 0 && index < numItems)
        {
            return items[index];
        }
        else
        {
            throw new ListIndexOutOfBoundsException("ListIndexOutOfBoundsException on get");
        }
    }

    @Override
    public void removeAll()
    {
        //Creates a new array and dereferences the old allowing garbage collection to cleanup
        items = new Object[MAX_LIST];
        numItems = 0;
    }
    
}
