/*
 * Implementation of StackADT using arrays
 */
package DyeToolkit;

/**
 *
 * @author Frank Dye
 */

public class StackArrayBased implements StackInterface
{
    final int MAX_STACK = 250;
    private Object items[];
    private int top;
    
    public StackArrayBased()
    {
        items = new Object[MAX_STACK];
        top = -1;
    }
    
    @Override
    public boolean isEmpty()
    {
        //Determines whether the stack is empty
        //PreCondition: None
        //PostCondition: Returns true if stack is empty otherwise returns false.
        return top < 0;
    }

    @Override
    public void popAll()
    {
        //Removes all items from the stack
        //PreCondition: None
        //PostCondition: Stack is empty.
        items = new Object[MAX_STACK];
        top = -1;
    }

    public boolean isFull()
    {
        return top == MAX_STACK-1;
    }
    
    @Override
    public void push(Object newItem) throws StackException
    {
        //Adds and item to the top of the stack
        //PreCondition: newItem != && is valid Object
        //PostCondition: On success insertion is on top of stack
        //Exception: Throws StackException if stack is full or cannot perform insertion
        if (!isFull())
        {
            items[++top] = newItem;
        }
        else
        {
            throw new StackException("StackException on " + "push: stack full");
        }
    }

    @Override
    public Object pop() throws StackException
    {
        //Removes and returns the top of the stack
        //PreCondition: None
        //PostCondition: If the stack is not empty, the top of the stack is returned and removed.
        //Exception: StackException thrown if stack is empty.
        
        if (!isEmpty())
        {
            return items[top--];
        }
        else
        {
            throw new StackException("Stack Exception " + 
                                     "pop: stack empty");
        }
    }

    @Override
    public Object peek() throws StackException
    {
        //Returns the top of the stack without removing it
        //PreCondition: None
        //PostCondition: If the stack is not empty the top of the stack is returned without removing it.
        //Exception: StackException thrown if stack is empty.
        
        if (!isEmpty())
        {
            return items[top];
        }
        else
        {
            throw new StackException("Stack Exception " + 
                                     "peek: stack empty");
        }
    }
    
}

